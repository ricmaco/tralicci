#!/usr/bin/env python

import argparse as cmdl
import numpy as np
import cv2


DESCRIPTION = '''Classifier test.'''


if __name__ == '__main__':
    parser = cmdl.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('-C', '--classifier', dest='classifier', action='store',
        help='filename *.xml ')
    parser.add_argument('-P', '--filename', dest='picture', action='store',
        help='image')
    args = parser.parse_args()
    
    if not args.classifier:
        print("missing '-C' classifier filename.")
        quit()

    img = cv2.imread(args.picture)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    classifier=cv2.CascadeClassifier(args.classifier)
    detectedObject = classifier.detectMultiScale(gray, 1.3, 5)
    while(True):
        for (x,y,w,h) in detectedObject:
           cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        cv2.imshow('img',img) 
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

