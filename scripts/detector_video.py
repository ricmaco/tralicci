#!/usr/bin/env python

import argparse as cmdl
import numpy as np
import cv2

DESCRIPTION = '''Classifier test.'''

def truncate(value):
    if value < 0:
        return 0
    elif value > 255:
        return 255
    return value

def contrast(value, channel):
    factor = (259 * (value + 255)) / (255 * (259 - value))
    return truncate(factor * (channel - 128) + 128)

if __name__ == '__main__':
    parser = cmdl.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('-C', '--classifier', dest='classifier', action='store',
        default='cascade.xml', help='filename *.xml')
    parser.add_argument('-d', '--device', dest='device', action='store',
        type=int, default='0', help='/dev/videoX device')
    args = parser.parse_args()
    
    # select video device
    cap = cv2.VideoCapture(args.device)
    # select cascade file
    classifier=cv2.CascadeClassifier(args.classifier)
    
    while(True):
      # read frame from capture device
      ret, frame = cap.read()
      # convert frame to gray
      gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
      detectedObject = classifier.detectMultiScale(gray, 1.05, 6)
      for (x,y,w,h) in detectedObject:
         cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 0, 255), 1)
      cv2.imshow('image', frame)

      if cv2.waitKey(1) & 0xFF == ord('q'):
          break
    cv2.destroyAllWindows()

