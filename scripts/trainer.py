#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Requirements:
   - Pillow
'''

from PIL import Image
from os import path
import argparse as cmdl
import os, errno
import subprocess as sp

DESCRIPTION = '''Generate a cascade xml trained to detect an image.'''

allowed = ('jpg', 'png', 'ppm', 'pgm', 'bmp', 'jpeg', 'jpe', 'gif', 'tiff')

pos_filename = 'pos.info'
neg_filename = 'bg.txt'
vec_filename = 'vector.vec'

def log(phase, string):
  print('[{}] {}.'.format(phase, string))

def image_size(image):
  '''
  Extracts image width and height and returns a whitespace separated string.
  '''
  im = Image.open(image)
  width, height = im.size
  return '{} {}'.format(width, height)

if __name__ == '__main__':
  parser = cmdl.ArgumentParser(description=DESCRIPTION)
  # with default
  parser.add_argument('-v', '--verbose', dest='verbosity', action='store_true',
    default=False, help='log what is happening')
  parser.add_argument('-D', '--pos-dir', dest='posdir', action='store',
    default='pos', help='directory where to analyze positive images')
  parser.add_argument('-d', '--neg-dir', dest='negdir', action='store',
    default='neg', help='directory where to analyze negative images')
  parser.add_argument('-o', '--out-dir', dest='outdir', action='store',
    default='cascade', help='name of the generated file')
  parser.add_argument('-f', '--feature-type', dest='type', action='store',
    default='HAAR', help='HAAR or LBP features')
  # without default
  parser.add_argument('-W', '--width', dest='width', action='store',
    help='width of the attention rectangle')
  parser.add_argument('-H', '--height', dest='height', action='store',
    help='height of the attention rectangle')
  parser.add_argument('-s', '--stages', dest='stages', action='store',
    help='number of stages of training')
  args = parser.parse_args()
  
  # test if important arguments are present
  if not args.width:
    raise ValueError('width (-W) not provided')
  if not args.height:
    raise ValueError('height (-H) not provided')
  if not args.stages:
    raise ValueError('number of stages (-s) not specified')
  
  # remove trailing '/' from directories if present
  if args.posdir[-1] == '/':
    args.posdir = args.posdir[:-1]
  if args.negdir[-1] == '/':
    args.negdir = args.negdir[:-1]
  
  # check if directories exists (and create outdir if necessary)
  if not path.isdir(args.posdir):
    raise OSError('invalid "{}" directory'.format(path.basename(args.posdir)))
  if not path.isdir(args.negdir):
    raise OSError('invalid "{}" directory'.format(path.basename(args.negdir)))
  try:
    os.makedirs(args.outdir)
  except OSError as exception:
    if exception.errno != errno.EEXIST:
      raise
  
  # get the list of positive and negative images and abort if one is empty
  pos_images = [x for x in os.listdir(args.posdir) \
    if x.lower().endswith(allowed)]
  pos_len = len(pos_images)
  if pos_len == 0:
    raise OSError('"{}" is an empty folder'.format(path.basename(args.posdir)))
  neg_images = [x for x in os.listdir(args.negdir) \
    if x.lower().endswith(allowed)]
  neg_len = len(neg_images)
  if neg_len == 0:
    raise OSError('"{}" is an empty folder'.format(path.basename(args.negdir)))
  
  # create positive images descriptor file
  with open(pos_filename, 'w') as outfile:
    for im in pos_images:
      # build line descripting current image
      line = '{}/{} 1 0 0 {}\n'.format(path.abspath(args.posdir), im,
        image_size('{}/{}'.format(args.posdir, im)))
      outfile.write(line)
  if args.verbosity:
    log(1, '"{}" file created'.format(pos_filename))
  
  # create negative images descriptor file
  with open(neg_filename, 'w') as outfile:
    for im in neg_images:
      # build line descripting current image
      outfile.write('{}/{}\n'.format(path.abspath(args.negdir), im))
  if args.verbosity:
    log(2, '"{}" file created'.format(neg_filename))
  
  # build the vector generation command and call it
  command = [
    'opencv_createsamples',
    '-info {}'.format(pos_filename),
    '-num {}'.format(pos_len),
    '-w {}'.format(args.width),
    '-h {}'.format(args.height),
    '-vec {}'.format(vec_filename)
  ]
  command_str = ' '.join(command)
  sp.check_call(command_str, shell=True)
  if args.verbosity:
    log(3, '"{}" file created'.format(vec_filename))
  
  # build the training command and call it
  command = [
    'opencv_traincascade',
    '-data {}'.format(args.outdir),
    '-vec {}'.format(vec_filename),
    '-bg {}'.format(neg_filename),
    '-numPos {}'.format(pos_len),
    '-numNeg {}'.format(neg_len),
    '-numStages {}'.format(args.stages),
    '-w {}'.format(args.width),
    '-h {}'.format(args.height),
    '-featureType {}'.format(args.type)
  ]
  command_str = ' '.join(command)
  sp.check_call(command_str, shell=True)
  if args.verbosity:
    log(4, 'Training completed')